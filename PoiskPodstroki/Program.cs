﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PoiskPodstroki
{
    class Program
    {

        //Хеш-функция для алгоритма Рабина-Карпа
        public static int Hash(string x)
        {
            int p = 31; //Простое число
            int rez = 0; //Результат вычисления 
            for (int i = 0; i < x.Length; i++)
            {
                rez += (int)Math.Pow(p, x.Length - 1 - i) * x[i];//Подсчет хеш-функции
            }
            return rez;
        }
        //Функция поиска алгоритмом Рабина-Карпа
        public static string Rabina(string x, string s)
        {
            string nom = ""; //Номера всех вхождений образца в строку
            if (x.Length > s.Length) return nom; //Если искомая строка больше исходной – возврат пустого поиска
            int xhash = Hash(x); //Вычисление хеш-функции искомой строки
            int shash = Hash(s.Substring(0, x.Length)); //Вычисление хеш-функции первого слова длины образца в строке S
            bool flag;
            int j;
            for (int i = 0; i < s.Length - x.Length; i++)
            {
                if (xhash == shash)//Если значения хеш-функций совпадают
                {
                    flag = true;
                    j = 0;
                    while ((flag == true) && (j < x.Length))
                    {
                        if (x[j] != s[i + j]) flag = false;
                        j++;
                    }
                    if (flag == true) //Если искомая строка совпала с частью исходной
                        nom = nom + Convert.ToString(i) + ", "; //Добавление номера вхождения
                }
                else //Иначе вычисление нового значения хеш-функции
                    shash = (shash - (int)Math.Pow(31, x.Length - 1) * s[i]) * 31 + (s[i + x.Length]);
            }
            if (nom != "") //Если вхождение найдено
            {
                nom = nom.Substring(0, nom.Length - 2); //Удаление запятой и пробела
            }
            return nom; //Возвращение результата поиска
        }

        private static void Pryamoi()
        {
            string[] a = new string[] {"1","2","3","4","56","1","22","44","27","67" };
            for (int i = 0; i < a.Length; i++)
            {
                Console.WriteLine("Строка №{0} : {1}", i + 1, a[i]);
            }
            Console.Write("\nИщем значение = ");

            char[] c = Console.ReadLine().ToCharArray();
            for (int i = 0; i < a.Length; i++) //строка
            {
                string[] temp = a[i].Split();
                for (int j = 0; j < temp.Length; j++)//каждое слово в строке
                {
                    int count = 0;//счетчик совпадения букв
                    foreach (char charr in temp[j])//проверка букв в искомом слове                     
                        for (int k = 0; k < c.Length; k++)
                            if (charr == c[k])
                            {
                                count++;
                                if (count == c.Length)//если кол-во букв искомого слова совпало, то слово найдено
                                    Console.WriteLine("найдено совпадение слова {0} в {1} строке", temp[j], i + 1);
                                break;
                            }
                }
            }
        }
        static void Main(string[] args)
        {
            Console.WriteLine("Rabina");
            string s = "123456789";
            string x = "5";
            Console.WriteLine(Rabina(x, s));
            Console.WriteLine("\nPryamoi");
            Pryamoi();
            Console.ReadKey();
        }
    }
}
